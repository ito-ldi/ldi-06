/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.complemento;

/**
 *
 * @author Luis
 */
public class BinarioConSignoADecimal{
     private String numBinario;
    
     public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado.substring(resultado.length() - getNumBinario().length(), resultado.length());
   } 
     public String complementoADos(String numBinario){
            String[] num = new String[numBinario.length()];
            int pos = 0;
            String invertirValores = "";
            String complementoADos = "";    
            for(int x = 0; x < numBinario.length();x++){
                 num[pos] = numBinario.substring(x, x+1);   
                  pos++;    
            }
               for(int y = 0; y < num.length;y++){
                 if(num[y].equals("1")){
                     num[y] = "0";
                 }else if(num[y].equals("0")){
                     num[y] = "1";
                 }
               }
             for(String y:num){
                 if(y != null){invertirValores +=y;}              
             }
             String uno = "00000001";
             System.out.println("Valores  invertidos       = "+invertirValores);
             System.out.println("mas 1 ---------------->     "+uno.substring(uno.length()-invertirValores.length(), uno.length()));
            complementoADos = sumaDeBinarios(invertirValores, "00000001");
         return complementoADos;
     }
     
     public String numBinarioADecimal(String numBinario){
        int pos = 0;
        int resultado = 0;
        int[] num = new int[numBinario.length()];   
        for(int x = numBinario.length(); x >0; x--){            
            num[pos] = (int) (Integer.parseInt(numBinario.substring(x-1, x))*(Math.pow(2, pos)));           
            resultado += num[pos];
            pos++;
        }        
        if(getNumBinario().substring(0,1).equals("0")){
            return "-"+resultado;
        }else{
            return resultado+"";
        }
        
    }
    
    public String getNumBinario() {
        return numBinario;
    }
    public void setNumBinario(String numBinario) {
        this.numBinario = numBinario;
    }
    
     public static void main(String[] args){
         BinarioConSignoADecimal invertir = new BinarioConSignoADecimal();
       
         System.out.println("Por ejemplo, el número binario con signo 11110000 tiene un 1 en el bit más alto,\n"
                            + " lo cual indica que es un entero negativo."
                            + " al pasarlo a complemento dos obtendremos un entero positivo\n");
         invertir.setNumBinario("11110000");
         System.out.println("Número binario a invertir = "+invertir.getNumBinario());           
         String complementoADos = invertir.complementoADos(invertir.getNumBinario());         
         System.out.println("El complemento a dos es   = "+complementoADos);    
         System.out.println("El número en decimal es   = "+invertir.numBinarioADecimal(complementoADos));
         
          System.out.println("\nAhora el complemento a dos de 11110000 nos a dado 00010000 el cual tiene un 0 en el bit más alto,\n"
                            + " lo cual indica que es un entero positivo."
                            + " al pasarlo a complemento dos obtendremos un entero negativo\n");
          
         System.out.println("\nEl complemento a dos es reversible aquí lo comprobamos");
         invertir.setNumBinario(complementoADos);
         System.out.println("Número binario a invertir = "+invertir.getNumBinario()); 
         complementoADos = invertir.complementoADos(invertir.getNumBinario()); 
         System.out.println("El complemento a dos es   = "+complementoADos);       
         System.out.println("El número en decimal es   = "+ invertir.numBinarioADecimal(invertir.getNumBinario()));
     }
    
}
