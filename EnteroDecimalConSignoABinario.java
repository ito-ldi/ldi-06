/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.complemento;

/**
 *
 * @author Luis
 */
public class EnteroDecimalConSignoABinario{
    private String numDecimal;
public String convercionDecimalABinario(String numDecimal){
            String resultado = "";
            String formato = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numBinario = new String[100];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 2;                
                          decimal = decimal /2;                
                    numBinario[x] = residuo+"";
                    x++;
                                  
              }while(decimal > 0);               
             for(int y = numBinario.length-1; y >= 0; y--){            
                if(!(numBinario[y] == null)){
                  resultado += numBinario[y];
                }
             }
             for(int u = resultado.length(); u < 8;u++){
                   formato += "0";
               }
        return formato+resultado;
    }

    public String complementoADos(String numBinario){        
            String[] num = new String[numBinario.length()];
            int pos = 0;
            String invertirValores = "";
            String complementoADos = "";    
            for(int x = 0; x < numBinario.length();x++){
                 num[pos] = numBinario.substring(x, x+1);   
                  pos++;    
            }
               for(int y = 0; y < num.length;y++){
                 if(num[y].equals("1")){
                     num[y] = "0";
                 }else if(num[y].equals("0")){
                     num[y] = "1";
                 }
               }
             for(String y:num){
                 if(y != null){invertirValores +=y;}              
             }
             String uno = "00000001";
             System.out.println("Valores  invertidos            = "+invertirValores);
             System.out.println("mas 1 ---------------->          "+uno.substring(uno.length()-invertirValores.length(), uno.length()));
             
            complementoADos = sumaDeBinarios(invertirValores, "00000001");
         return complementoADos.substring(complementoADos.length()-8, complementoADos.length());
     }   
          public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
   }     
    
    public String getNumDecimal() {
        return numDecimal;
    }
    public void setNumDecimal(String numDecimal) {
        this.numDecimal = numDecimal;
    }
    
    public void convertir(String numDecimal){
        this.numDecimal = numDecimal;
        numDecimal = (Math.abs(Integer.parseInt(numDecimal)))+"";
         
        if(getNumDecimal().substring(0,1).equals("-")){               
            System.out.println("Número decimal a convertir a Binario = "+getNumDecimal()
                              +"\nPrimero se convierte a formato binario = "+convercionDecimalABinario(numDecimal)
                              +"\nComo el número decimal es negativo se saca su complemento a Dos\n");
                              String comADos = complementoADos(convercionDecimalABinario(numDecimal));
            
            System.out.println("Número decimal = "+getNumDecimal()+" A binario = "+ comADos);
        }else{
            System.out.println("Número decimal a convertir a Binario = "+getNumDecimal());
            System.out.println("Número decimal = "+getNumDecimal()+" A binario = "+ convercionDecimalABinario(getNumDecimal()));
            
        }
    }
    public static void main(String[] args){
        EnteroDecimalConSignoABinario conversion = new EnteroDecimalConSignoABinario();
        System.out.println("Ejemplo con número negativo\n");
        conversion.setNumDecimal("-43");
        conversion.convertir(conversion.getNumDecimal());
        
        System.out.println("\nEjemplo con número positivo\n");
        conversion.setNumDecimal("43");
        conversion.convertir(conversion.getNumDecimal());
    }
}
